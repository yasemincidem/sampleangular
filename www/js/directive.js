﻿angular.module("sampleApp.Directives", [])
.directive("headerBar", function () {
	return {
		restrict: 'E',
		templateUrl: 'templates/headerBar.html',
		scope: {
			imagesInfo: '=info'
		}
	}
});